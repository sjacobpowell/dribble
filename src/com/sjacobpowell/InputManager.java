package com.sjacobpowell;

import static com.sjacobpowell.DribbleComponent.HEIGHT;
import static com.sjacobpowell.DribbleComponent.WIDTH;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class InputManager implements KeyListener, MouseMotionListener {
	public boolean[] keys = new boolean[KeyEvent.CHAR_UNDEFINED];
	public Point mousePosition = new Point(WIDTH / 2, HEIGHT / 2);

	@Override
	public void keyPressed(KeyEvent e) {
		setKey(e.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		setKey(e.getKeyCode(), false);
	}

	private void setKey(int code, boolean pressed) {
		if (code > 0 && code < keys.length) {
			keys[code] = pressed;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		mousePosition = new Point(e.getX(), e.getY());
	}

}
