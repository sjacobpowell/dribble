package com.sjacobpowell;

public class Ball {
	public double x;
	public double y;
	public double radius;
	public double ySpeed = 0;
	public double xSpeed = 0;
	
	public Ball(double x, double y, double radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	public void tick() {
		x += xSpeed;
		y += ySpeed;
	}
	
}
