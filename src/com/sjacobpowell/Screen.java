package com.sjacobpowell;

import static com.sjacobpowell.DribbleComponent.SCALE;

import java.util.List;

public class Screen extends Bitmap {
	private Bitmap slinky = getSlinky();
	private Bitmap platform = getPlatform();
	
	public Screen(int width, int height) {
		super(width, height);		
	}
	
	public void render(Game game) {
		clear();
		drawSlinky(game);
		drawBalls(game.balls);
	}

	private void drawBalls(List<Ball> balls) {
		balls.forEach(ball -> {
			Bitmap bitmap = new Bitmap(0, 0);
			if(balls.size() > 0) {
				bitmap = new Bitmap((int)ball.radius, (int)ball.radius);
				for(int i = 0; i < bitmap.pixels.length; i++) {
					bitmap.pixels[i] = 0x990000;
				}
				bitmap = ArtTools.circlefy(bitmap, ball.radius / 2.2, 0x333333, 3);
			}
			draw(bitmap, (int)ball.x, (int)ball.y);
		});
	}

	private void drawSlinky(Game game) {
		int BODY_RENDER_COUNT = 250;
		double x =game.platform.getX() / SCALE;
		double y = game.platform.getY() / SCALE;
		double slinkyBase = (width - slinky.width) / 2;
		double deltaX = (x - slinkyBase) / BODY_RENDER_COUNT;
		double deltaY = (y + height) / BODY_RENDER_COUNT;
		double currentX = x;
		double currentY = y;
		for(int i = 0; i < BODY_RENDER_COUNT; i++) {
			if(i < 1) {
				draw(platform, (int)(currentX - platform.width / 2), (int)(currentY - platform.height / 2));
			} else {
				draw(slinky, (int)(currentX - slinky.width / 2), (int)currentY);
			}
			currentX -= deltaX;
			currentY += deltaY;
		}
	}

	private Bitmap getPlatform() {
		int woodColor = 0xC8A165;
		int edgeColor = 0x663300;
		Bitmap platform = new Bitmap(40, 10); 
		for(int i = 0; i < platform.pixels.length; i++) {
			if(i % platform.height == 0 || i % platform.width  == 0) {
				platform.pixels[i] = edgeColor;
			} else {
				platform.pixels[i] = woodColor;
			}
		}
		return platform;
	}

	private Bitmap getSlinky() {
		Bitmap slinky = new Bitmap(16, 16);
		for(int i = 0; i < slinky.pixels.length; i++) {
			slinky.pixels[i] = 0xB0C4DE;
		}
		return ArtTools.circlefy(slinky, slinky.width / 2, 0x58626F, 2);
	}

	@Override
	public void clear() {
		for(int i = 0; i < pixels.length; i++) {
			pixels[i] = 0x99ccff;
		}
	}

}
