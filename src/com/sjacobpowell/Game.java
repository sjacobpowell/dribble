package com.sjacobpowell;

import static com.sjacobpowell.DribbleComponent.HEIGHT;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class Game {
	public int time;
	public Point platform = new Point(0, 0);
	public List<Ball> balls;
	private static final double GRAVITY = 0.1;

	public Game() {
		balls = new ArrayList<Ball>();
		for(int i = 0; i < 20; i++) {
			balls.add(new Ball(i + i * i * 2, i + i * i * 2, i * 1));
		}
	}

	public void tick(InputManager inputManager) {
		platform = inputManager.mousePosition;
		tickBalls();
		time++;
	}

	private void tickBalls() {
		for(int i = 0; i < balls.size(); i++) {
			if(collision(balls.get(i)) && balls.get(i).ySpeed > 0) {
				balls.get(i).ySpeed = -balls.get(i).ySpeed * .8;
			} else {
				balls.get(i).ySpeed += GRAVITY;
			}
			balls.get(i).tick();
			if(balls.get(i).y >= HEIGHT) {
				balls.get(i).y = 0;
				balls.get(i).ySpeed = 0;
			}
		}
	}

	private boolean collision(Ball ball) {
		boolean yCollision = Math.abs(platform.y - (ball.y + ball.radius)) < 3;
		boolean xCollision = (ball.x + ball.radius * 2) >= platform.x && ball.x <= (platform.x + 20);
		return xCollision && yCollision;
	}
}
